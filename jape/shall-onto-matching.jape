// Trouver les shall
// cf: https://gate.ac.uk/sale/tao/splitap7.html#x39-743000G
Phase: FindLookup
Input: Lookup Token
Options: control = appelt
Rule: ShallLookup
(
	{Lookup.class == "SafetyRule"}
	({Token.category == "RB"})?
	{Token.category == "VB"} 
	({Token.category == "RB"})?
	({Token.category == "VBN"})?
):shall
-->:shall.Shall = {class = :shall.Lookup.class}


Phase: FindLookup
Input: Lookup Token
Options: control = appelt
Rule: SafetyThingLookup
(
	{
		Lookup.class == "LookupThing", 
		Lookup.type == "instance",
		Token.category != "VBN"
	}
):thing
-->:thing.LookupThing = {class = :thing.Lookup.class, inst = :thing.Lookup.inst, isFromNorm = :thing.Lookup.isFromNorm}

Phase: FindLookup
Input: Lookup Token
Options: control = appelt
Rule: ComparatorLookup
(
	{ Lookup.class == "Comparator", Lookup.type == "instance", Token.string != "within" } | 
	(
		({Token.string == "not"})? ({Token.category == "VB"})? {Token.category == "JJR"} {Token.string == "than"}
	)
):comparator
-->:comparator.Comparator = {class = :comparator.Lookup.class, inst = :comparator.Lookup.inst}

Phase: FindLookup
Input: Lookup Token Measurement
Options: control = appelt
Rule: ComparatorLookupWithinCorrection
({ Lookup.class == "Comparator", Lookup.type == "instance", Token.string == "within" }):comparator
({Measurement})
-->:comparator.Comparator = {class = :comparator.Lookup.class, inst = :comparator.Lookup.inst}

/////////
// I67 //
/////////

Phase: FindI67LookupThing
Input: LookupThing
Options: control = appelt
Rule: FindI67LookupThing
(
	{
		LookupThing.isFromNorm =~ "I-67"
	}
):thing
-->:thing.LookupThingI67 = {class = :thing.LookupThing.class, inst = :thing.LookupThing.inst, isFromNorm = :thing.LookupThing.isFromNorm}

Phase: ShallFeaturing
Input: Sentence LookupThingI67 Shall
Options: control = appelt
Rule: FindI67Shall
(
	{Sentence contains Shall, Sentence contains LookupThingI67}
):sentence
//ShallI67
-->:sentence{
	AnnotationSet sentenceAS = (AnnotationSet) bindings.get("sentence");
	AnnotationSet shallAS = inputAS.get("Shall", sentenceAS.firstNode().getOffset(), sentenceAS.lastNode().getOffset());
	Annotation sentence = sentenceAS.iterator().next();
	Annotation shall = shallAS.iterator().next();
	AnnotationSet thingAS = inputAS.get("LookupThingI67", sentenceAS.firstNode().getOffset(), sentenceAS.lastNode().getOffset());
	Set<String> thingURIs = new HashSet();
	List<String> things = new ArrayList();
	for (Annotation thing: thingAS) {
		things.add(gate.Utils.stringFor(doc, thing));
		thingURIs.add((String) thing.getFeatures().get("class"));
	}
	
	if (thingURIs.size() >= 2) {
		FeatureMap features = Factory.newFeatureMap(); 
		shall.getFeatures().put("isI67", "true");
		outputAS.add(sentence.getStartNode(), sentence.getEndNode(), "ShallI67",  features);

		/*System.out.println("==============");
		System.out.println("nb thingAS: " + String.valueOf(thingAS.size()));
		System.out.println("thingAS: " + String.join(";",things));
		System.out.println("sentence: " + gate.Utils.stringFor(doc, sentence));
		System.out.println("==============");*/
	}
}


// cf: https://gate.ac.uk/wiki/jape-repository/annotations.html#section-1.
Phase: AnalyzeSentence
Input: Sentence Shall
Options: control = appelt
Rule: ShallFeaturing
(
	{Sentence contains Shall}
):sentence
-->
:sentence{
	AnnotationSet sentenceAS = (AnnotationSet) bindings.get("sentence");
	AnnotationSet shallAS = inputAS.get("Shall", sentenceAS.firstNode().getOffset(), sentenceAS.lastNode().getOffset());
	Node sentenceStart = sentenceAS.firstNode();
	Node sentenceEnd = sentenceAS.lastNode();
	Node startShall = shallAS.firstNode();

	try {
		// System.out.println("============");

		////// SUBJECT //////
		String possibleSubject = doc.getContent().getContent(sentenceStart.getOffset(), startShall.getOffset()).toString();
		AnnotationSet subjectAS = inputAS.get("LookupThing", sentenceStart.getOffset(), startShall.getOffset());
		String subject = "";
		if (subjectAS != null && subjectAS.size()>0) {
			subject = (String) subjectAS.iterator().next().getFeatures().get("inst");
		}
		////// END SUBJECT //////

		List<Annotation> shallList = shallAS.inDocumentOrder();
		Iterator<Annotation> itr = shallList.iterator();
		Iterator<Annotation> itr2 = shallList.iterator();
		itr2.next();
		while (itr.hasNext()) {
			Annotation shallA = itr.next();
			// System.out.println(gate.Utils.stringFor(doc, shallA));
			
			////// OBJECT //////
			Long objectEnd;
			if (itr2.hasNext()) {
				Annotation nextShallA = itr2.next();
				objectEnd = nextShallA.getStartNode().getOffset();
			}
			else {
				objectEnd = sentenceEnd.getOffset();
			}
			String possibleObject = doc.getContent().getContent(shallA.getEndNode().getOffset(), objectEnd).toString();
			AnnotationSet objectAS = inputAS.get("LookupThing", shallA.getEndNode().getOffset(), objectEnd);
			String object = "";
			if (objectAS != null && objectAS.size()>0) {
				object = (String) objectAS.iterator().next().getFeatures().get("inst");
				shallA.getFeatures().put("hasObject", object);
			}
			// Measurement
			AnnotationSet measureAS = inputAS.get("Measurement", shallA.getEndNode().getOffset(), objectEnd);

			Double value = 0.0;
			if (measureAS != null && measureAS.size()>0) {
				Annotation measureA = measureAS.iterator().next();
				String unit = (String) measureA.getFeatures().get("unit");
				String dimension = (String) measureA.getFeatures().get("dimension");
				value = (Double) measureA.getFeatures().get("value");
				shallA.getFeatures().put("hasValue", value);
				shallA.getFeatures().put("hasUnit", unit);
				shallA.getFeatures().put("hasDimension", dimension);
			}

			// Comparator
			AnnotationSet comparatorAS = inputAS.get("Comparator", shallA.getEndNode().getOffset(), objectEnd);
			if (comparatorAS != null && comparatorAS.size()>0) {
				Annotation comparatorA = comparatorAS.iterator().next();
				String comparator = (String) gate.Utils.stringFor(doc, comparatorA);
				shallA.getFeatures().put("hasComparator", comparator);
			}

			////// END OBJECT ////// 

			shallA.getFeatures().put("hasVerb", gate.Utils.stringFor(doc, shallA));
			shallA.getFeatures().put("possibleSubject", possibleSubject);
			shallA.getFeatures().put("possibleObject", possibleObject);
			if (subject.length() > 0)
				shallA.getFeatures().put("hasSubject", subject);
			shallA.getFeatures().put("fromSentence", gate.Utils.stringFor(doc, sentenceAS));
			shallA.getFeatures().put("shallOffset", startShall.getOffset());
			shallA.getFeatures().put("fromDocument", doc.getName());

		}
	} catch(InvalidOffsetException e) {
		System.err.println(e);
		System.err.println(gate.Utils.stringFor(doc, sentenceAS));
	}
}




Phase: OntologyPopuling
Input: Shall
Options: control = appelt

Rule: OntologyShallPopuling
(
	{Shall}
):shall
-->
:shall{
	AnnotationSet shallAS = (AnnotationSet) bindings.get("shall");
	Annotation shallA = shallAS.iterator().next();
	String shallId = "rule"+String.valueOf(shallAS.firstNode().getOffset());
	String object = (String) shallA.getFeatures().get("hasObject");
	String subject = (String) shallA.getFeatures().get("hasSubject");
	String unit = (String) shallA.getFeatures().get("hasUnit");
	String comparator = (String) shallA.getFeatures().get("hasComparator");
	Double value = (Double) shallA.getFeatures().get("hasValue");

	// uncomment me to have the empty results !
	if (object == null && subject == null && unit == null && comparator == null) {
		return;
	}

	// get class "SafetyRule"
	OClass shallClass = ontology.getOClass(ontology.createOURIForName("SafetyRule")); 
	if(shallClass == null) { 
		System.err.println("Error: class SafetyRule does not exist!"); 
		return; 
	}

	OURI shallURI = ontology.createOURIForName(OUtils.toResourceName(shallId));
	OInstance shallInst = ontology.getOInstance(shallURI);

	if (shallInst == null) {
		try {	
			if (shallA.getFeatures().containsKey("isI67")) {
				OClass shallI67Class = ontology.getOClass(ontology.createOURIForName("I-67")); 
				shallInst = ontology.addOInstance(shallURI, shallI67Class);
			}
			else {
				shallInst = ontology.addOInstance(shallURI, shallClass);
			}

			///////////////////
			// General info  //
			///////////////////

			DatatypeProperty dataProp = ontology.getDatatypeProperty(ontology.createOURIForName("fromSentence"));
			Literal literal = new Literal((String) shallA.getFeatures().get("fromSentence"));
			shallInst.addDatatypePropertyValue(dataProp, literal);

			dataProp = ontology.getDatatypeProperty(ontology.createOURIForName("hasVerb"));
			literal = new Literal((String) shallA.getFeatures().get("hasVerb"));
			shallInst.addDatatypePropertyValue(dataProp, literal);

			dataProp = ontology.getDatatypeProperty(ontology.createOURIForName("possibleSubject"));
			literal = new Literal((String) shallA.getFeatures().get("possibleSubject"));
			shallInst.addDatatypePropertyValue(dataProp, literal);

			dataProp = ontology.getDatatypeProperty(ontology.createOURIForName("possibleObject"));
			literal = new Literal((String) shallA.getFeatures().get("possibleObject"));
			shallInst.addDatatypePropertyValue(dataProp, literal);

			dataProp = ontology.getDatatypeProperty(ontology.createOURIForName("fromDocument"));
			literal = new Literal((String) shallA.getFeatures().get("fromDocument"));
			shallInst.addDatatypePropertyValue(dataProp, literal);


			///////////////////////
			// Object & Subject  //
			///////////////////////

			
			if (object != null) {
				OURI objectURI = ontology.createOURI(object);
				OInstance objectInst = ontology.getOInstance(objectURI);
				if (objectInst != null) {
					ObjectProperty objectProp = ontology.getObjectProperty(ontology.createOURIForName("hasObject"));
					shallInst.addObjectPropertyValue(objectProp, objectInst);
				}
				else {
					System.err.println("Instance of object not find in ontology: "+objectURI);
				}
			}

			
			if (subject != null) {
				OURI subjectURI = ontology.createOURI(subject);
				OInstance subjectInst = ontology.getOInstance(subjectURI);
				if (subjectInst != null) {
					ObjectProperty subjectProp = ontology.getObjectProperty(ontology.createOURIForName("hasSubject"));
					shallInst.addObjectPropertyValue(subjectProp, subjectInst);
				}
				else {
					System.err.println("Instance of object not find in ontology: "+subjectURI);
				}
			}


			/////////////////
			// Comparator  //
			/////////////////
			
			if (comparator != null && comparator.length() > 0) {
				// Class "Comparator"
				OClass comparatorClass = ontology.getOClass(ontology.createOURIForName("Comparator"));
				if(comparatorClass == null) { 
					System.err.println("Error: class Comparator does not exist!"); 
					return; 
				}

				// New/Get instance of "Comparator"
				OURI comparatorURI = ontology.createOURIForName(OUtils.toResourceName(comparator));
				OInstance comparatorInst = ontology.getOInstance(comparatorURI);
				if (comparatorInst == null) {
					try {	
						comparatorInst = ontology.addOInstance(comparatorURI, comparatorClass);
						comparatorInst.setLabel(comparator, OConstants.ENGLISH);
					}
					catch(NullPointerException e) {
						System.err.println("NullPointerException "+comparator);
					}
				}
				ObjectProperty objectProp = ontology.getObjectProperty(ontology.createOURIForName("hasComparator"));
				shallInst.addObjectPropertyValue(objectProp, comparatorInst);
			}

			/////////////////////////
			// Dimension and unit  //
			/////////////////////////

			
			if (value != null) {
				dataProp = ontology.getDatatypeProperty(ontology.createOURIForName("hasValue"));
				literal = new Literal(String.valueOf(value)); // double ?
				shallInst.addDatatypePropertyValue(dataProp, literal);
			}

			
			if (unit != null && unit.length() > 0) {
				// dirty way to remove plural
				if (unit != null && unit.length() > 3 && unit.charAt(unit.length() - 1) == 's') {
		      unit = unit.substring(0, unit.length() - 1);
		    }

				// Class "Unit"
				OClass unitClass = ontology.getOClass(ontology.createOURIForName("Unit"));
				if(unitClass == null) { 
					System.err.println("Error: class Unit does not exist!"); 
					return; 
				}

				// Class "Dimension"
				String dimension = (String) shallA.getFeatures().get("hasDimension");
				OClass dimensionClass = null;
				if (dimension != null) {
					OURI dimOURI = ontology.createOURIForName(OUtils.toResourceName(dimension));
					dimensionClass = ontology.getOClass(dimOURI);
					if (dimensionClass == null) {
						try {
							dimensionClass = ontology.addOClass(dimOURI);
							dimensionClass.setLabel(dimension, OConstants.ENGLISH);
							unitClass.addSubClass(dimensionClass);
						}
						catch (NullPointerException e) {
							System.err.println("NullPointerException "+dimension);
						}
					}
				}

				// New instance of "Unit" or "Dimension"
				OURI unitURI = ontology.createOURIForName(OUtils.toResourceName(unit));
				OInstance unitInst = ontology.getOInstance(unitURI);
				if (unitInst == null) {
					try {	
						unitInst = ontology.addOInstance(unitURI, (dimension!=null)?dimensionClass:unitClass);
						unitInst.setLabel(unit, OConstants.ENGLISH);
					}
					catch(NullPointerException e) {
						System.err.println("NullPointerException "+unit);
						System.err.println((dimension!=null)?dimensionClass:unitClass);
					}
				}
				ObjectProperty objectProp = ontology.getObjectProperty(ontology.createOURIForName("hasUnit"));
				shallInst.addObjectPropertyValue(objectProp, unitInst);
			}
		}
		catch(NullPointerException | InvalidValueException e) {
			System.err.println(e);
		}	
	}

}