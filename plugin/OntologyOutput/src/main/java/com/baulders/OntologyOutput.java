package com.baulders;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Document;
import gate.Resource;
import gate.creole.AbstractLanguageAnalyser;
import gate.creole.ExecutionException;
import gate.creole.ExecutionInterruptedException;
import gate.creole.ResourceInstantiationException;
import gate.creole.metadata.*;
import gate.creole.ontology.*;
// import gate.creole.ontology.OConstants;
// import gate.creole.ontology.OClass;
// import gate.creole.ontology.OInstance;

import org.apache.log4j.Logger;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.net.URISyntaxException;
import java.io.IOException;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.io.PrintWriter;

@CreoleResource(
    name = "Ontology Output",
    comment = "Export your current ontology into a file XML/RDF")
public class OntologyOutput extends AbstractLanguageAnalyser {

  private static final Logger log = Logger.getLogger(OntologyOutput.class);

  /**
   * The ontology to export (LR)
   */
  private Ontology ontology = null;

  public Ontology getOntology() {
    return this.ontology;
  }

  @RunTime
  @CreoleParameter(comment = "The ontology to export (LR)")
  public void setOntology(Ontology ontology) {
    this.ontology = ontology;
  }

  /**
   * The output file (OWL)
   */
  private URL owlFileURL = null;

  public URL getOwlFileURL() {
    return this.owlFileURL;
  }

  @RunTime
  @CreoleParameter(comment = "The output file")
  public void setOwlFileURL(URL owlFileURL){
    this.owlFileURL = owlFileURL;
  }

  /**
   * The output rules file (CSV)
   */
  private URL csvRulesFileURL = null;

  public URL getCsvRulesFileURL() {
    return this.csvRulesFileURL;
  }

  @RunTime
  @CreoleParameter(comment = "The output rules file (CSV)")
  public void setCsvRulesFileURL(URL csvRulesFileURL){
    this.csvRulesFileURL = csvRulesFileURL;
  }

  /**
   * The output UIC measures file (CSV)
   */
  private URL txtUicMeasuresFileURL = null;

  public URL getTxtUicMeasuresFileURL() {
    return this.txtUicMeasuresFileURL;
  }

  @RunTime
  @CreoleParameter(comment = "The output UIC measures file (txt)")
  public void setTxtUicMeasuresFileURL(URL txtUicMeasuresFileURL){
    this.txtUicMeasuresFileURL = txtUicMeasuresFileURL;
  }
  

  /**
   * Initialize this Ontology Output.
   * @return this resource.
   * @throws ResourceInstantiationException if an error occurs during init.
   */
  public Resource init() throws ResourceInstantiationException {
    return this;
  }

  /**
   * Execute this Ontology Output over the current document.
   * @throws ExecutionException if an error occurs during processing.
   */
  public void execute() throws ExecutionException {
    // log.info("Ceci est un msg d'info");
    
    // open output file
    File owlFile = null;
    try {
      owlFile = new File(owlFileURL.toURI());
    }
    catch(URISyntaxException e) {
      throw new ExecutionException(e);
    }
    
    // write output file OWL
    Writer writer = null;
    try {
      writer = new OutputStreamWriter(new FileOutputStream(owlFile), "UTF-8");
      ontology.writeOntologyData(writer, OConstants.OntologyFormat.RDFXML, true);      
    } catch (IOException ex) {
      throw new ExecutionException("Could not open writer for file " + owlFile.getAbsolutePath(), ex);
    } finally {
      try {
        if(writer != null) writer.close();
      } catch(IOException e) {
        throw new ExecutionException("Could not close writer for file " + owlFile.getAbsolutePath(), e);
      }
    }

    // write rules CSV
    OClass shallClass = ontology.getOClass(ontology.createOURIForName("SafetyRule")); 
    OClass shallI67Class = ontology.getOClass(ontology.createOURIForName("I-67")); 
    Set<OInstance> shallInstances = ontology.getOInstances(shallClass, OConstants.Closure.TRANSITIVE_CLOSURE);
    
    
    List<String[]> dataLines = new ArrayList<>();
    dataLines.add(new String[] {"fromDocument", "normURI", "hasSubject", "hasObject", "hasComparator", "hasValue", "hasUnit", "maybeIsI67", "fromSentence", "possibleSubject", "possibleObject"});

    DatatypeProperty fromDocumentProp = ontology.getDatatypeProperty(ontology.createOURIForName("fromDocument"));
    ObjectProperty hasSubjectProp = ontology.getObjectProperty(ontology.createOURIForName("hasSubject"));
    ObjectProperty hasObjectProp = ontology.getObjectProperty(ontology.createOURIForName("hasObject"));
    ObjectProperty hasComparatorProp = ontology.getObjectProperty(ontology.createOURIForName("hasComparator"));
    DatatypeProperty hasValueProp = ontology.getDatatypeProperty(ontology.createOURIForName("hasValue"));
    ObjectProperty hasUnitProp = ontology.getObjectProperty(ontology.createOURIForName("hasUnit"));
    DatatypeProperty fromSentenceProp = ontology.getDatatypeProperty(ontology.createOURIForName("fromSentence"));
    DatatypeProperty possibleSubjectProp = ontology.getDatatypeProperty(ontology.createOURIForName("possibleSubject"));
    DatatypeProperty possibleObjectProp = ontology.getDatatypeProperty(ontology.createOURIForName("possibleObject"));

    for (OInstance shall : shallInstances) {
      String fromDocument = OntologyOutput.getDatatypePropertyValue(shall, fromDocumentProp);
      String normURI = shall.getOURI().getResourceName(); 
      String hasSubject = OntologyOutput.getObjectPropertyValue(shall, hasSubjectProp);
      String hasObject = OntologyOutput.getObjectPropertyValue(shall, hasObjectProp);
      String hasComparator = OntologyOutput.getObjectPropertyValue(shall, hasComparatorProp);
      String hasValue = OntologyOutput.getDatatypePropertyValue(shall, hasValueProp);
      String hasUnit = OntologyOutput.getObjectPropertyValue(shall, hasUnitProp);
      String maybeIsI67 = "";
      if (shall.isInstanceOf(shallI67Class, OConstants.Closure.DIRECT_CLOSURE)) {
        maybeIsI67 = "yes";
      }
      String fromSentence = OntologyOutput.getDatatypePropertyValue(shall, fromSentenceProp).replaceAll("\\R", "");
      String possibleSubject = OntologyOutput.getDatatypePropertyValue(shall, possibleSubjectProp).replaceAll("\\R", "");
      String possibleObject = OntologyOutput.getDatatypePropertyValue(shall, possibleObjectProp).replaceAll("\\R", "");
      dataLines.add(new String[] {fromDocument, normURI, hasSubject, hasObject, hasComparator, hasValue, hasUnit, maybeIsI67, fromSentence, possibleSubject, possibleObject});
    }
    createCsvFile(csvRulesFileURL, dataLines);

    // Write uic measures TXT
    Document doc = getDocument();
    AnnotationSet annotations = doc.getAnnotations();
    List<Annotation> measureList = annotations.get("uicMeasure").inDocumentOrder();
    AnnotationSet refsAS = annotations.get("uicMeasureRef");

    List<String> txt = new ArrayList<>();
    for (Annotation measureA : measureList) {
      String[] refsList = ((String)measureA.getFeatures().get("refsList")).split(";");
      if (refsList.length > 0) {
        // because if split a empty string
        if (refsList[0].trim().length() > 0) {
          txt.add(((String)measureA.getFeatures().get("name"))+ ":");
          for( int i = 0; i < refsList.length; i++) {
            Annotation ref = refsAS.get(Integer.parseInt(refsList[i]));
            String line = "    - ";
            line += "["+(String)ref.getFeatures().get("name")+"]";
            line += " ";
            line += (String)ref.getFeatures().get("context");
            txt.add(line);
          }
          txt.add("");
        }
      }
      
    }
    createTxtFile(txtUicMeasuresFileURL, txt);
  }

  static String getDatatypePropertyValue(OInstance shall, DatatypeProperty property) {
    List<Literal> literals = shall.getDatatypePropertyValues(property);
    if (literals.size() > 0)
      return literals.get(0).getValue();
    return "";
  }

  static String getObjectPropertyValue(OInstance shall, ObjectProperty property) {
    List<OInstance> instances = shall.getObjectPropertyValues(property);
    if (instances.size() > 0)
      return instances.get(0).getOURI().getResourceName();
    return "";
  }

  public static String convertToCSV(String[] data) {
    return Stream.of(data)
      .map(OntologyOutput::escapeSpecialCharacters)
      .collect(Collectors.joining(","));
  }

  public void createCsvFile(URL fileURL, List<String[]> dataLines) throws ExecutionException {
    PrintWriter pw = null;
    try {
      File csvOutputFile = new File(fileURL.toURI());
      pw = new PrintWriter(csvOutputFile);
      dataLines.stream().map(OntologyOutput::convertToCSV)
        .forEach(pw::println);
    } catch (FileNotFoundException | URISyntaxException e) {
      throw new ExecutionException("Could not open writer for file " + fileURL.toString(), e);
    }
    finally {
      if (pw != null) pw.close();
    }
  }

  public void createTxtFile(URL fileURL, List<String> lines) throws ExecutionException {
    PrintWriter pw = null;
    try {
      File txtOutputFile = new File(fileURL.toURI());
      pw = new PrintWriter(txtOutputFile);
      lines.stream().map(OntologyOutput::removeNewLinesCharacters).forEach(pw::println);
    } catch (FileNotFoundException | URISyntaxException e) {
      throw new ExecutionException("Could not open writer for file " + fileURL.toString(), e);
    }
    finally {
      if (pw != null) pw.close();
    }
  }

  public static String removeNewLinesCharacters(String data) {
    return data.replaceAll("\\R", "").replaceAll("\\t", "");
  }

  public static String escapeSpecialCharacters(String data) {
    String escapedData = data.replaceAll("\\R", "");
    if (data.contains(",") || data.contains("\"") || data.contains("'")) {
      data = data.replace("\"", "\"\"");
      escapedData = "\"" + data + "\"";
    }
    return escapedData;
  }
}

