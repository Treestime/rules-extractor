package com.baulders;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Document;
import gate.Resource;
import gate.creole.AbstractLanguageAnalyser;
import gate.creole.ExecutionException;
import gate.creole.ExecutionInterruptedException;
import gate.creole.ResourceInstantiationException;
import gate.creole.metadata.*;
import gate.creole.ontology.Ontology;
import gate.creole.ontology.OConstants;

import org.apache.log4j.Logger;
import java.io.FileInputStream;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.net.URISyntaxException;
import java.io.IOException;
import gate.creole.ontology.*;
import java.util.Set;

@CreoleResource(
    name = "Ontology Reset",
    comment = "Reset your ontology (clean and reload)")
public class OntologyReset extends AbstractLanguageAnalyser {

  private static final Logger log = Logger.getLogger(OntologyReset.class);

  /**
   * The ontology to reset (LR)
   */
  private Ontology ontology = null;

  public Ontology getOntology() {
    return this.ontology;
  }

  @RunTime
  @CreoleParameter(comment = "The ontology to reset (LR)")
  public void setOntology(Ontology ontology) {
    this.ontology = ontology;
  }

  /**
   * The file to import into the ontology
   */

  private URL fileURL = null;
  private File file = null;
  private FileInputStream fileIn = null;

  public URL getFileURL() {
    return this.fileURL;
  }

  @RunTime
  @CreoleParameter(comment = "The file to import into the ontology")
  public void setFileURL(URL fileURL){
    this.fileURL = fileURL;
  }
  

  /**
   * Initialize this Ontology Reset.
   * @return this resource.
   * @throws ResourceInstantiationException if an error occurs during init.
   */
  public Resource init() throws ResourceInstantiationException {
    return this;
  }

  /**
   * Execute this Ontology Reset over the current document.
   * @throws ExecutionException if an error occurs during processing.
   */
  public void execute() throws ExecutionException {
    // check the interrupt flag before we start - in a long-running PR it is
    // good practice to check this flag at appropriate key points in the
    // execution, to allow the user to interrupt processing if it takes too
    // long.
    /*if(isInterrupted()) {
      throw new ExecutionInterruptedException("Execution of Ontology Reset has been interrupted!");
    }
    interrupted = false; */
    try {
      this.file = new File(fileURL.toURI());
    }
    catch(URISyntaxException e) {
      throw new ExecutionException(e);
    }
    try {
      this.fileIn = new FileInputStream(this.file);
    }
    catch(IOException e) {
      throw new ExecutionException(e);
    }
    ontology.cleanOntology();
    //log.debug("Ontology is cleaned");
    ontology.readOntologyData(fileIn, OConstants.ONTOLOGY_DEFAULT_BASE_URI, OConstants.OntologyFormat.RDFXML, true);
    // OClass i67Class = ontology.getOClass(ontology.createOURIForName("I-67")); 
    // // Set<OClass> i67classes = i67Class.getSubClasses(OConstants.Closure.DIRECT_CLOSURE);
    // Set<OInstance> i67classes = ontology.getOInstances(i67Class, OConstants.Closure.DIRECT_CLOSURE);
    // /*for (OClass c: i67classes) {
    //   lookupthings.addAll(ontology.getOInstances(c, OConstants.Closure.DIRECT_CLOSURE))
    // }*/
    // for (OInstance inst: i67classes) {
    //   System.out.println(inst.getOURI());
    // }

    /*
    - récupérer les instances(lookup) de chaque chaques instances de I-67
    - enregistrer les OURI dans le corpus des rules (nb: corpus en entrée de la PR)
    - dans jape: on peut checker si le lookup thing est dans la liste de la feature du corpus
     */
  }

}

