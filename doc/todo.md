




A faire (fonctionalités) :

- Améliorer l'heuristique de l'analyse syntaxique (ex: shall by => forme passive, bouge le sujet)
- Repérer les titres (html)
- Gérer listes et tableaux (html)
- Finir la boucle d'auto-apprentissage des units (annoter dans le document les unités présentes dans l'ontologie et pas seulement celles du module "Measurement")

Pistes :

- Plutôt qu'un sujet et un objet, faut-il avoir un sujet et des themes ?
- Question: faut-il matcher les classes ? Que faire de ça ?