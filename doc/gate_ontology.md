# Gate: Semantic Annotation and Ontologies

Documentation: https://gate.ac.uk/sale/tao/splitch14.html#x19-35100014

## Load Ontology

__Plugin:__ Ontology (8.5-SNAPSHOT) https://jenkins.gate.ac.uk/job/gate-plugins/job/gateplugin-Ontology/job/master/

* __Load local ontology:__ New Language Ressources > OWLIM Ontology
* __Load distant ontology:__ ConnectSesameOntology LR > http://uid:pwd@host.com:8080/openrdf-sesame/repositories/someid
* __Fusions locals ontologies:__ Load 1 ontology, then right click on the ontology > Load, select the 2nd ontology, save 

## Ontology Viewer/Editor

(To edit, better use Protégé)

__Plugin:__ Ontology Tools (8.6) on maven with :

> Group: uk.ac.gate.plugins
> Artifact: ontology-tools
> Version: 8.6-SNAPSHOT

Reminder:
- Datatype Properties: Person [Domain] has_age [Property] xsd:nonNegativeInteger [Data] 
- Object Properties: Person [Domain] work_for [Property] Organisation [Range]
- No domain = owl:Thing
- Several domains = intersection

## Ontology Annotation Tool

__Plugin:__ Ontology Tools (8.6)

Semantic Annotation = link text mentions to ontology resources (mention annotations have a feature (inst) with the URI of the resource)

## OntoRoot Gazetteer

## Ontology-enabled JAPE

## LKB Gazetteer

## Ontology-based evaluation

Ontology Population (add new facts to a given ontology) != Ontology Learning (create or extend the structure of the ontology)