shall (if verb actif)
=====================


if several shall
----------------
subject 1 max
(
	shall verb*
	object 1 max + 1 measurement max
) +

OR

if 1 shall and several verbs
----------------------------
subject
shall 
(
	verb* (if be => + adjectif)
	object
) +


shall ... by (if verb passif)
=============================

object
shall 
verb* (if be => + adjectif)
subject


subject
======

si ce n'est pas it, aller chercher dans owl ?


object
======

not less than
the same as ?
measure
at least 
a minimum



In order to/to + (verb)+ : goal
if ?
Within ?
will ?
where ?
