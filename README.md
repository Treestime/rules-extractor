## Installation

Prérequis : 

- Gate 8.6
- Windows 8.1/10
- Java jdk-8u261-windows-x64
- Maven apache-maven-3.6.3-bin

Lancer le script install.bat pour déployer les plugins.

## Corpus

Le corpus est en html et anglais.

- Document Word => Sauvegarder comme "html filtré" 

Cela permet de mieux découper les phrases.

## Utilisation

1. Lancer Gate
2. Clic droit sur Application > "Restore application frome file"
3. Sélectionner "main.gapp"
4. Dans l'application "Main", cliquer sur "Run the application". L'application génère un "output.owl".